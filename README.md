# bookmark-merge

The aim is to create a tool that merges pdf and leaves bookmarks in the first page of each PDF. And well, it is a good way to learn something of bash scripts.

### Dependancies

It is needed `python 3.*` and above. It is needed `pdftk` installed on the machine. If the error `unexpected end of file` occurs, you will need `dos2unix` to set the end of row from `crlf` to `lf`.