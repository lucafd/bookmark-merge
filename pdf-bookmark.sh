#!/bin/bash
find $(pwd) -name "bookmarked.pdf" -delete
echo '1' >info.txt
for i in *.pdf; do pdftk "$i" dump_data | grep NumberOfPages | sed 's/[^0-9]*//' >> info.txt; done
for file in *.pdf; do echo "${file%.*}" >> name.txt; done
python3 extraction.py
pdftk *pdf output row.pdf
pdftk row.pdf dump_data output info.txt
sed -e "/^NumberOfPages/r bookmark.txt" info.txt | sed -e "s/,//g" > id.txt
pdftk row.pdf update_info id.txt output bookmarked.pdf
rm info.txt bookmark.txt row.pdf id.txt name.txt
exit 0 
# var1="$(pwd)"; file="${var1}/bookmarked.pdf"; if [[ -f "$file" ]]; then echo "neve"; else echo "mama"; fi
# serve a qualcosa? non so però all'inizio mi sembrava sensato
